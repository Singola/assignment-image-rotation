#include "bmp_tools.h"

static const uint16_t BMP_FILE_SIGNATURE = 19778;
static const uint16_t BITS_PER_PIXEL = 24;
static const uint32_t HEADER_SIZE = 54; // == (uint32_t) sizeof(struct bmp_header) if struct bmp_header is packed
static const uint32_t DIB_HEADER_SIZE = 40;
static const uint16_t PLANES_NUM = 1;

struct __attribute__((packed)) bmp_header
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        //width and height in px
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount; // bits per px, 24 always, CHECK!!!
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

/* STATIC CONTENT */

static uint32_t img_size_in_bytes(uint32_t width, uint32_t height, uint32_t padding) {
	return width*height*3 + padding*height;
}

static struct bmp_header create_bmp_header(uint32_t width, uint32_t height, uint32_t padding) {
	uint32_t img_size = img_size_in_bytes(width, height, padding);
	return (struct bmp_header) {
		.bfType = BMP_FILE_SIGNATURE,
        .bfileSize = HEADER_SIZE + img_size,
        .bfReserved = 0,
        .bOffBits = HEADER_SIZE,
        .biSize = DIB_HEADER_SIZE,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = PLANES_NUM,
        .biBitCount = BITS_PER_PIXEL,
        .biCompression = 0,
        .biSizeImage = img_size,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
	};
}

static uint32_t padding_in_bytes(uint32_t width_in_px) {
	if (width_in_px % 4 == 0) return 0;
	return ((width_in_px*3 / 4) + 1)*4 - width_in_px*3;
}

static struct bmp_header read_bmp_header(FILE* file) {
	struct bmp_header header = {0};
	const size_t blocks_count = fread(&header, sizeof(struct bmp_header), 1, file);
	if (blocks_count != 1) return (struct bmp_header) {0};
	return header;
}

static struct pixel* read_pixels(FILE* file, uint32_t width_in_px, uint32_t height_in_px, uint32_t padding_in_bytes) {
	struct pixel* px_array = malloc(sizeof(struct pixel) * width_in_px * height_in_px);
	struct pixel* position_to_write = px_array;
	for (uint32_t curr_line = 0; curr_line < height_in_px; curr_line++) {
		size_t px_count = fread(position_to_write, sizeof(struct pixel), width_in_px, file);
		if (px_count != width_in_px) {
			free(px_array);
			return NULL;
		}
		int fseek_success = fseek(file, padding_in_bytes, SEEK_CUR);
		if (!(fseek_success == 0) ) {
			free(px_array);
			return NULL;
		}
		position_to_write = position_to_write + (size_t) width_in_px; 
	}
	return px_array;
}


static enum write_status write_pixels_in_add_mode(FILE* file, struct pixel* px_array, uint32_t width_in_px, uint32_t height_in_px, uint32_t padding_in_bytes) {
	uint32_t curr_line = 0;
	struct pixel* position_to_read_from = px_array;
	while (curr_line < height_in_px) {
		size_t px_count = fwrite(position_to_read_from, sizeof(struct pixel), width_in_px, file);
		if (px_count != width_in_px) {
			return WRITE_ERR;
		}
		// write padding (trash bytes, don't care about them)
		size_t padding_bytes_count = fwrite((void*) position_to_read_from, 1, padding_in_bytes, file);
		if (padding_bytes_count != padding_in_bytes ) {
			return WRITE_ERR;
		}
		curr_line++;
		position_to_read_from = position_to_read_from + (size_t) width_in_px;
	}
	return WRITE_OK;
}

static enum read_status validate_bmp_header(const struct bmp_header* header) {
	if (header->bfType != BMP_FILE_SIGNATURE) {
		return READ_INVALID_SIGNATURE;
	}
	if (header->bOffBits != HEADER_SIZE) {
		return READ_INVALID_HEADER_SIZE;
	}
	if (header->biBitCount != BITS_PER_PIXEL) {
		return READ_INVALID_PIXEL_SIZE;
	}
	return READ_OK;
}

/*EXPORTED CONTENT*/

enum read_status from_bmp( FILE* in, struct image* img) {
	
	struct bmp_header header = read_bmp_header(in);
	enum read_status header_validation = validate_bmp_header(&header);
	if (header_validation != READ_OK) return header_validation;
	
	const uint32_t width = header.biWidth;
	const uint32_t height = header.biHeight;
	const uint32_t padding = padding_in_bytes(width);
	
	struct pixel* px_array = read_pixels(in, width, height, padding);
	if (px_array == NULL) {
		return READ_INVALID_BITS;
	}
	
	img_height_set(img, (uint64_t) height);
	img_width_set(img, (uint64_t) width);
	img_pixels_set(img, px_array);
	return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
	
	uint32_t width = (uint32_t) img_width_get(img);
	uint32_t height = (uint32_t) img_height_get(img);
	uint32_t padding = padding_in_bytes(width);
	struct pixel* px_array = img_pixels_get(img);
	
	struct bmp_header header = create_bmp_header(width, height, padding);
	
	// IF FILE IS OPENED IN ADD MODE THEN NO PROBLEM
	const size_t written_count = fwrite(&header, sizeof(struct bmp_header), 1, out);
	if (written_count != 1) return WRITE_ERR;
	enum write_status px_are_written = write_pixels_in_add_mode(out, px_array, width, height, padding);
	
	return px_are_written;
}
