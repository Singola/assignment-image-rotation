#include "transformation.h"

static uint64_t calculate_y_position(uint64_t curr_x) {
	return curr_x;
}

static uint64_t calculate_x_position(uint64_t curr_y, uint64_t height) {
	return height - 1 - curr_y;
}

/*EXPORTED FUNCTIONS*/

struct image rotate( struct image const source ) {
	
	struct pixel* src_px_array = img_pixels_get(&source);
	uint64_t src_px_height = img_height_get(&source);
	uint64_t src_px_width = img_width_get(&source);
	uint64_t array_size = src_px_width*src_px_height;
	
	struct pixel* new_px_array = malloc(sizeof(struct pixel) * array_size);
	
	for (uint64_t i = 0; i < array_size; i++) {
		
		const uint64_t new_y = calculate_y_position(i % src_px_width);
		const uint64_t new_x = calculate_x_position(i / src_px_width, src_px_height);
		new_px_array[new_y*src_px_height + new_x] = src_px_array[i];
	}
	
	return img_create(src_px_height, src_px_width, new_px_array);
}
