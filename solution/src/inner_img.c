#include "inner_img.h"

struct image img_create_default() {
	return (struct image) {0};
}

struct image img_create(uint64_t width, uint64_t height, struct pixel* data) {
	return (struct image) {.width = width, .height = height, .data = data};
}

void img_pixels_destroy_in_memory(struct image* img) {
	free(img->data);
}

struct pixel* img_pixels_get(const struct image* img) {
	return img->data;
}

uint64_t img_width_get(const struct image* img) {
	return img->width;
}

uint64_t img_height_get(const struct image* img) {
	return img->height;
}

void img_pixels_set( struct image* img, struct pixel* array) {
	img->data = array;
}

void img_width_set( struct image* img, uint64_t width) {
	img->width = width;
}

void img_height_set( struct image* img, uint64_t height) {
	img->height = height;
}
