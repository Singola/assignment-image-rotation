#include "io.h"

FILE* open_file_in_rb_mode(const char* filename) {
	return fopen(filename, "rb");
}

FILE* open_file_in_ab_mode(const char* filename) {
	return fopen(filename, "ab");
}

void close_file(FILE* file) {
	if (file != NULL) fclose(file);
}

void remove_file(const char* filename) {
	remove(filename);
}


