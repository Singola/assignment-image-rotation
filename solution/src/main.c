#include "bmp_tools.h"
#include "inner_img.h"
#include "io.h"
#include "transformation.h"
#include <stdbool.h>

static const char* read_status_messages[] = {
	[READ_OK] = "Data was successfully read\n",
	[READ_INVALID_SIGNATURE] = "Invalid signature!\n",
	[READ_INVALID_BITS] = "Invalid pixel data!\n",
	[READ_INVALID_HEADER_SIZE] = "Invalid header size!\n",
	[READ_INVALID_PIXEL_SIZE] = "Invalid pixel size!\n"
};

static const char* write_status_messages[] = {
	[WRITE_OK] = "Data was written successfully\n",
	[WRITE_ERR] = "Error while writing to file occured\n"
};

static const char* open_status_messages[] = {
	[OPEN_OK] = "Provided file: %s - successfully opened\n",
	[OPEN_ERR] = "Provided file: %s - cannot be opened\n"
};

static const char* WRONG_USAGE_MSG = "Usage: %s <input_file>.bmp <output_file>.bmp\n";

static void print_msg_to_stderr(const char* msg) {
	fprintf(stderr, "err: %s", msg);
}

static void print_msg_to_stdout(const char* msg) {
	printf("%s", msg);
}

static void print_msg(bool error, const char* msg){

	if (error) {
		print_msg_to_stderr(msg);
	} else {
		print_msg_to_stdout(msg);
	}
}

static void display_opening_msg(FILE* file, char* filename) {
	if (file == NULL) {
		fprintf(stderr, open_status_messages[OPEN_ERR], filename);
	}
}

int main( int argc, char** argv ) {

    if (argc != 3) {
		printf(WRONG_USAGE_MSG, argv[0]);
	} else {
		FILE* in = open_file_in_rb_mode(argv[1]);
		remove_file(argv[2]);
		FILE* out = open_file_in_ab_mode(argv[2]);
		
		if (in != NULL && out != NULL) {
			struct image img = img_create_default();
			enum read_status rs = from_bmp( in, &img);
			print_msg(rs != READ_OK, read_status_messages[rs]);
			if (rs == READ_OK) {
				struct image new_img = rotate(img);
				enum write_status ws = to_bmp( out, &new_img);
				print_msg(ws != WRITE_OK, write_status_messages[rs]);
				img_pixels_destroy_in_memory(&img);
				img_pixels_destroy_in_memory(&new_img);
			}
		} else {
			display_opening_msg(in, argv[1]);
			display_opening_msg(out, argv[2]);
		}
		
		close_file(in);
		close_file(out);
	}
    
    return 0;
}
