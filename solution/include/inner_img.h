#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#ifndef STRUCTS
#define STRUCTS
struct image {
 uint64_t width, height;
 struct pixel* data;
};

struct __attribute__((packed)) pixel { uint8_t b, g, r; };
#endif

struct image img_create_default();

struct image img_create(uint64_t width, uint64_t height, struct pixel* data);

void img_pixels_destroy_in_memory(struct image* img);

struct pixel* img_pixels_get(const struct image* img);

uint64_t img_width_get(const struct image* img);

uint64_t img_height_get(const struct image* img);

void img_pixels_set( struct image* img, struct pixel* array);

void img_width_set( struct image* img, uint64_t width);

void img_height_set( struct image* img, uint64_t height);
