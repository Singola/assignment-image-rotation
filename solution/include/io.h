#include <stdio.h>

enum open_status {
 OPEN_OK = 0,
 OPEN_ERR
};

FILE* open_file_in_rb_mode(const char* filename);

FILE* open_file_in_ab_mode(const char* filename);

void close_file(FILE* file);

void remove_file(const char* filename);
